import Vue from 'vue'
import App from '@/App'
import router from '@/router'
import store from '@/store'
import Snotify, { SnotifyPosition } from 'vue-snotify'
import VueTimeago from 'vue-timeago'

Vue.use(VueTimeago, {
  name: 'timeago', // component name, `timeago` by default
  locale: 'en-US',
  locales: {
    // you will need json-loader in webpack 1
    'en-US': require('vue-timeago/locales/en-US.json')
  }
})

const options = {
  toast: {
    position: SnotifyPosition.centerTop
  }
}

Vue.use(Snotify, options)

/* eslint-disable no-new */

new Vue({
  name: 'app',
  el: '#app',
  router,
  store,
  render: h => h(App)
})
