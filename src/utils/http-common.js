import axios from 'axios'
import { PROD_API_BASE_URL, DEV_API_BASE_URL } from '@/config'

export const HTTP = axios.create({
  baseURL: process.env.NODE_ENV === 'production' ? PROD_API_BASE_URL : DEV_API_BASE_URL
})
