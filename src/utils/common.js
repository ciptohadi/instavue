/*
common utility functions
*/
import router from '@/router'
import store from '@/store'

const loadComponent = (file, module = '', isComponent = false) => {
  let path = (module === '') ? 'views' : isComponent ? `modules/${module}/_components` : `modules/${module}/_views`

  return () => import(`@/${path}/${file}.vue`)
}

/*
  sample output, exp => expired date
  {
  "iss": "https://instaclone.dev/account/login",
  "iat": 1511939361,
  "exp": 1511942961,
  "nbf": 1511939361,
  "jti": "G1WyNVNcllLBjFgo",
  "sub": 2,
  "prv": "87e0af1ef9fd15812fdec97153a14e0b047546aa"
}
  */
const jwtDecode = () => {
  let token = store.getters.authToken
  let props = ['exp', 'sub']
  let propsCount = props.length

  let tokenDecoded = JSON.parse(atob(token.split('.')[1]))

  let retVal = tokenDecoded

  for (let i = 0; i < propsCount; i++) {
    if (!tokenDecoded[props[i]]) {
      retVal = null
      break
    }
  }

  return retVal
}

const isTokenExpired = () => {
  let currentTime = Date.now() / 1000
  return (jwtDecode().exp < currentTime) ? 1 : 0
}

const checkAuth = (to, from, next) => {
  let token = store.getters.authToken
  if (!token || isTokenExpired()) {
    router.push({ name: 'welcome' })
  } else {
    next()
  }
}

const FormDataToJSON = (FormElement) => {
  let formData = new FormData(FormElement)
  let ConvertedJSON = {}
  for (const [key, value] of formData.entries()) {
    ConvertedJSON[key] = value
  }

  return ConvertedJSON
}

const JSONToFormData = (JSONObj) => {
  let formData = new FormData()

  for (let key in JSONObj) {
    formData.append(key, JSONObj[key])
  }

  return formData
}

const isAuthUser = (id) => {
  let token = store.getters.authToken

  return token ? id === jwtDecode().sub : false
}

export {
  loadComponent,
  jwtDecode,
  isTokenExpired,
  checkAuth,
  FormDataToJSON,
  JSONToFormData,
  isAuthUser
}
