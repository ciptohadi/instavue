/* routes for photos feature */
import { loadComponent, checkAuth } from '@/utils/common'

const module = 'Account'

export default [
  {
    path: '/u/:id',
    name: 'user.show',
    component: loadComponent('Profile', module),
    beforeEnter: (to, from, next) => { checkAuth(to, from, next) }
  },
  { path: '/profile',
    name: 'profile',
    component: loadComponent('Profile', module),
    beforeEnter: (to, from, next) => { checkAuth(to, from, next) }
  },
  { path: '/register', name: 'register', component: loadComponent('Register', module) },
  { path: '/login', name: 'login', component: loadComponent('Login', module) },
  { path: '/settings',
    component: loadComponent('Settings', module),
    children: [
      {
        path: '',
        name: 'settings',
        components: {
          AccountUserAvatar: loadComponent('AccountUserAvatar', module, true),
          AccountUserDetails: loadComponent('AccountUserDetails', module, true)
        }
      },
      {
        path: '/user-avatar-update',
        name: 'user-avatar-update',
        components: {
          AccountUserAvatar: loadComponent('AccountUserAvatarUpdate', module, true)
        }
      },
      {
        path: '/user-details-update',
        name: 'user-details-update',
        components: {
          AccountUserDetails: loadComponent('AccountUserDetailsUpdate', module, true)
        }
      }
    ],
    beforeEnter: (to, from, next) => { checkAuth(to, from, next) }
  }
]
