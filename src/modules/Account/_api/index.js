import { HTTP } from '@/utils/http-common'

const getAccountDetails = (id, cb, errCb, config = {}) => (
  HTTP.get(`users/${id}?include=photos`, config)
    .then(response => {
      cb(response)
    })
    .catch(error => {
      errCb(error)
    })
)

const registerAccount = (registerAccountData, cb, errCb) => {
  HTTP.post(`account/register`, registerAccountData)
  .then(response => {
    cb(response)
  })
  .catch(error => {
    errCb(error)
  })
}

const loginAccount = (loginAccountData, cb, errCb) => {
  HTTP.post(`account/login`, loginAccountData)
  .then(response => {
    cb(response)
  })
  .catch(error => {
    errCb(error)
  })
}

const logoutAccount = (cb, errCb, config = {}) => {
  HTTP.delete(`account/logout`, config)
  .then(response => {
    cb(response)
  })
  .catch(error => {
    errCb(error)
  })
}

const updateAccountUserAvatar = (updateAcountUserAvatarData, cb, errCb, config = {}) => {
  HTTP.post(`account/me/update-avatar`, updateAcountUserAvatarData, config)
  .then(response => {
    cb(response)
  })
  .catch(error => {
    errCb(error)
  })
}

const updateAccountUserDetails = (updateAccountUserDetailsData, cb, errCb, config = {}) => {
  HTTP.post(`account/me`, updateAccountUserDetailsData, config)
  .then(response => {
    cb(response)
  })
  .catch(error => {
    errCb(error)
  })
}

export default {
  getAccountDetails, registerAccount, loginAccount, logoutAccount, updateAccountUserAvatar, updateAccountUserDetails
}
