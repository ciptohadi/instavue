import AccountDetails from './_components/AccountDetails'
import AccountRegister from './_components/AccountRegister'
import AccountLogin from './_components/AccountLogin'
// import AccountUpdate from './_components/AccountUpdate'
import store from '@/store'
import accountStore from './_store'

if (!store.$_account) store.registerModule('$_account', accountStore)

export { AccountDetails, AccountRegister, AccountLogin }
