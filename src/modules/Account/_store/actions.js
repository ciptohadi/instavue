import * as types from './mutation-types'
import api from '../_api'
import router from '@/router'
import { LikeModule } from '@/modules/Like'
import { FollowModule } from '@/modules/Follow'
import { CommentModule } from '@/modules/Comment'

export default {
  getAccountDetails ({ commit, rootState }, id) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }

    commit(types.GET_ACCOUNT_DETAILS_REQUEST)

    api.getAccountDetails(
      id,
      (response) => {
        commit(types.GET_ACCOUNT_DETAILS_SUCCESS, response)
      },
      (error) => {
        commit(types.GET_ACCOUNT_DETAILS_FAILURE, error)
        if (error.response) {
          router.push({name: 'notfound'})
        } else {
          router.push({name: 'error'})
        }
      },
      { headers }
    )
  },

  registerAccount ({ dispatch, commit, state }, registerAccountData) {
    commit(types.REGISTER_ACCOUNT_REQUEST)

    api.registerAccount(
      registerAccountData,
      (response) => {
        commit(types.REGISTER_ACCOUNT_SUCCESS, response)
        this._vm.$snotify.success(response.data.message.content)
        dispatch('loginAccount', registerAccountData)
      },
      (error) => {
        commit('ERROR_CAUGHT', null, { root: true })
        commit(types.REGISTER_ACCOUNT_FAILURE, error)
      }
    )
  },

  loginAccount ({ commit, state, rootState }, loginAccountData) {
    commit(types.LOGIN_ACCOUNT_REQUEST)

    api.loginAccount(
      loginAccountData,
      (response) => {
        if (rootState.$_account) {
          commit(types.RESET_ACCOUNT_DATA)
        }
        if (rootState.$_photo) {
          commit('$_photo/RESET_PHOTO_DATA', null, { root: true })
        }
        if (rootState.$_comment) {
          commit('$_comment/RESET_COMMENTS', null, { root: true })
          commit('$_comment/RESET_CHILD_COMMENTS', null, { root: true })
        }
        commit('AUTH_TOKEN_SET', response, { root: true })
        commit(types.LOGIN_ACCOUNT_SUCCESS, response)
        this._vm.$snotify.success(response.data.message.content)

        if (!rootState.$_like) LikeModule.register()
        if (!rootState.$_follow) FollowModule.register()
        if (!rootState.$_comment) CommentModule.register()

        router.push({ name: 'home' })
      },
      (error) => {
        commit('ERROR_CAUGHT', null, { root: true })
        commit(types.LOGIN_ACCOUNT_FAILURE, error)
      }
    )
  },

  logoutAccount ({ commit, rootState }) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }

    commit(types.LOGOUT_ACCOUNT_REQUEST)

    api.logoutAccount(
      (response) => {
        commit('AUTH_TOKEN_UNSET', null, { root: true })
        commit(types.LOGOUT_ACCOUNT_SUCCESS, response)

        this._vm.$snotify.success(response.data.message.content)
        router.push({ name: 'welcome' })
      },
      (error) => {
        commit('ERROR_CAUGHT', null, { root: true })
        commit(types.LOGOUT_ACCOUNT_FAILURE, error)
        this._vm.$snotify.error(error.response.data.error.message)
      },
      { headers }
    )
  },

  updateAccountUserAvatar ({ commit, rootState }, updateAccountUserAvatarData) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }

    commit(types.UPDATE_ACCOUNT_USER_AVATAR_REQUEST)

    api.updateAccountUserAvatar(
      updateAccountUserAvatarData,
      (response) => {
        commit(types.UPDATE_ACCOUNT_USER_AVATAR_SUCCESS, response)
        commit(
          'UPDATE_STATE_OBJECT',
          {module: '$_account', stateObjectKey: 'user', updatedObject: response.data.user},
          { root: true }
        )
        // commit(
        //   'DATA_CHANGED_SET',
        //   { user: { id: response.data.user.id } },
        //   { root: true }
        // )
        this._vm.$snotify.success(response.data.message.content)
        router.push({ name: 'settings' })
      },
      (error) => {
        commit('ERROR_CAUGHT', null, { root: true })
        commit(types.UPDATE_ACCOUNT_USER_AVATAR_FAILURE, error)
      },
      { headers }
    )
  },

  updateAccountUserDetails ({ commit, rootState }, updateAccountUserDetailsData) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }

    commit(types.UPDATE_ACCOUNT_USER_DETAILS_REQUEST)

    api.updateAccountUserDetails(
      updateAccountUserDetailsData,
      (response) => {
        commit(types.UPDATE_ACCOUNT_USER_DETAILS_SUCCESS, response)
        commit(
          'UPDATE_STATE_OBJECT',
          {module: '$_account', stateObjectKey: 'user', updatedObject: response.data.user},
          { root: true }
        )
        // commit(
        //   'DATA_CHANGED_SET',
        //   { user: { id: response.data.user.id } },
        //   { root: true }
        // )
        this._vm.$snotify.success(response.data.message.content)
        router.push({ name: 'settings' })
      },
      (error) => {
        commit('ERROR_CAUGHT', null, { root: true })
        commit(types.UPDATE_ACCOUNT_USER_DETAILS_FAILURE, error)
      },
      { headers }
    )
  }
}
