import * as types from './mutation-types'

export default {
  /* ACCOUNT DETAILS */
  [types.GET_ACCOUNT_DETAILS_REQUEST] (state) {
    state.getAccountDetailsStatus = null
    state.user = null
  },

  [types.GET_ACCOUNT_DETAILS_SUCCESS] (state, response) {
    state.getAccountDetailsStatus = 'Successfully get account details!'
    state.user = response.data.user
  },

  [types.GET_ACCOUNT_DETAILS_FAILURE] (state, error) {
    state.getAccountDetailsStatus = error.response.data.error.message
    state.user = null
  },

  /* ACCOUNT REGISTRATION */
  [types.REGISTER_ACCOUNT_REQUEST] (state) {
    state.registerAccountStatus = null
  },

  [types.REGISTER_ACCOUNT_SUCCESS] (state, response) {
    state.registerAccountStatus = response.data.message.content
  },

  [types.REGISTER_ACCOUNT_FAILURE] (state, error) {
    let status = 'Something error. Try again or contact admin!'

    let data = error.response.data

    if (data.error) {
      status = data.error.message
    } else if (data.meta.message === 'VALIDATION_FAILED') {
      // validation errors
      status = data.errors
    }

    state.registerAccountStatus = status
  },

  /* ACCOUNT LOGIN */
  [types.LOGIN_ACCOUNT_REQUEST] (state) {
    state.loginAccountStatus = null
  },

  [types.LOGIN_ACCOUNT_SUCCESS] (state, response) {
    state.loginAccountStatus = response.data.message.content
  },

  [types.LOGIN_ACCOUNT_FAILURE] (state, error) {
    state.loginAccountStatus = error.response.data.error.message
  },

  /* ACCOUNT LOGOUT */
  [types.LOGOUT_ACCOUNT_REQUEST] (state) {
    state.logoutAccountStatus = null
  },

  [types.LOGOUT_ACCOUNT_SUCCESS] (state, response) {
    state.logoutAccountStatus = response.data.message.content
  },

  [types.LOGOUT_ACCOUNT_FAILURE] (state, error) {
    state.logoutAccountStatus = error.response.data.error.message
  },

  /* ACCOUNT USER AVATAR UPDATE */
  [types.UPDATE_ACCOUNT_USER_AVATAR_REQUEST] (state) {
    state.updateAccountUserAvatarStatus = null
  },

  [types.UPDATE_ACCOUNT_USER_AVATAR_SUCCESS] (state, response) {
    state.updateAccountUserAvatarStatus = response.data.message.content
    state.user = response.data.user
  },

  [types.UPDATE_ACCOUNT_USER_AVATAR_FAILURE] (state, error) {
    let status = 'Something error. Try again or contact admin!'

    let data = error.response.data

    if (data.error) {
      status = data.error.message
    } else if (data.meta.message === 'VALIDATION_FAILED') {
      // validation errors
      status = data.errors
    }

    state.updateAccountUserAvatarStatus = status
  },

  /* ACCOUNT USER DETAILS UPDATE */
  [types.UPDATE_ACCOUNT_USER_DETAILS_REQUEST] (state) {
    state.updateAccountUserDetailsStatus = null
  },

  [types.UPDATE_ACCOUNT_USER_DETAILS_SUCCESS] (state, response) {
    state.updateAccountUserDetailsStatus = response.data.message.content
    state.user = response.data.user
  },

  [types.UPDATE_ACCOUNT_USER_DETAILS_FAILURE] (state, error) {
    let status = 'Something error. Try again or contact admin!'

    let data = error.response.data

    if (data.error) {
      status = data.error.message
    } else if (data.meta.message === 'VALIDATION_FAILED') {
      // validation errors
      status = data.errors
    }

    state.updateAccountUserDetailsStatus = status
  },

  [types.RESET_ACCOUNT_DATA] (state) {
    state.user = null
  }
}
