const stateAccountDetails = {
  getAccountDetailsStatus: null,
  user: null
}

const stateAccountRegister = {
  registerAccountStatus: null
}

const stateAccountLogin = {
  loginAccountStatus: null
}

const stateAccountLogout = {
  logoutAccountStatus: null
}

const stateAccountUserDetailsUpdate = {
  updateAccountUserDetailsStatus: null
}

const stateAccountUserAvatarUpdate = {
  updateAccountUserAvatarStatus: null
}

export default Object.assign(
  stateAccountDetails,
  stateAccountRegister,
  stateAccountLogin,
  stateAccountLogout,
  stateAccountUserDetailsUpdate,
  stateAccountUserAvatarUpdate
)
