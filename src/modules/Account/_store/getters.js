const user = state => state.user

const getAccountDetailsStatus = state => state.getAccountDetailsStatus
const registerAccountStatus = state => state.registerAccountStatus
const loginAccountStatus = state => state.loginAccountStatus
const logoutAccountStatus = state => state.logoutAccountStatus
const updateAccountUserDetailsStatus = state => state.updateAccountUserDetailsStatus
const updateAccountUserAvatarStatus = state => state.updateAccountUserAvatarStatus

export default {
  user, getAccountDetailsStatus, registerAccountStatus, loginAccountStatus, logoutAccountStatus, updateAccountUserDetailsStatus, updateAccountUserAvatarStatus
}
