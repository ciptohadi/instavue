import { HTTP } from '@/utils/http-common'

const getFollowings = (id, cursor = '', direction = '', config = {}) => (
  HTTP.get(`users/${id}/followings?cursor=${cursor}&direction=${direction}`, config)
    .then((response) => { return response })
    .catch((error) => { throw error })
)

const getFollowers = (id, cursor = '', direction = '', config = {}) => (
  HTTP.get(`users/${id}/followers?cursor=${cursor}&direction=${direction}`, config)
    .then((response) => { return response })
    .catch((error) => { throw error })
)

const storeFollowing = (id, cb, errCb, config = {}) => {
  HTTP.post(`users/${id}/follow`, [], config)
  .then(response => {
    cb(response)
  })
  .catch(error => {
    errCb(error)
  })
}

const deleteFollowing = (id, cb, errCb, config = {}) => {
  HTTP.delete(`users/${id}/unfollow`, config)
  .then(response => {
    cb(response)
  })
  .catch(error => {
    errCb(error)
  })
}

export default {
  getFollowings, getFollowers, storeFollowing, deleteFollowing
}
