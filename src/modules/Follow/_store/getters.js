const followings = state => state.followings
const followingsCompleted = state => state.followingsCompleted
const followingsCursor = state => state.followingsCursor
const getFollowingsStatus = state => state.getFollowingsStatus

const followers = state => state.followers
const followersCompleted = state => state.followersCompleted
const followersCursor = state => state.followersCursor
const getFollowersStatus = state => state.getFollowersStatus

const storeFollowingStatus = state => state.storeFollowingStatus
const deleteFollowingStatus = state => state.deleteFollowingStatus

export default {
  followings,
  followingsCompleted,
  followingsCursor,
  getFollowingsStatus,
  followers,
  followersCompleted,
  followersCursor,
  getFollowersStatus,
  storeFollowingStatus,
  deleteFollowingStatus
}
