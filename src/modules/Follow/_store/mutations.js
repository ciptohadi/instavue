import * as types from './mutation-types'

export default {
  /* FOLLOWING LIST */
  [types.GET_FOLLOWINGS_REQUEST] (state) {
    state.getFollowingsStatus = null
  },

  [types.GET_FOLLOWINGS_SUCCESS] (state, response) {
    state.getFollowingsStatus = 'Successfully get  all photo likes!'

    let data = response.data

    if (state.followingsCompleted) {
      return
    } else if (!data.followings) {
      state.getFollowingsStatus = data.message.content
      state.followingsCompleted = true
      if (data.meta) {
        state.followingsCursor = Object.assign(state.followingsCursor, {
          direction: data.meta.direction
        })
      }

      return
    }

    state.followings = state.followings.concat(data.followings)
    state.followingsCursor = Object.assign(state.followingsCursor, {
      current: data.meta.cursor.current,
      next: data.meta.cursor.next,
      prev: data.meta.cursor.prev,
      count: data.meta.cursor.count
    })

    if (state.followingsCursor.count < state.followingsCursor.limit) {
      state.followingsCompleted = true
    }
  },

  [types.GET_FOLLOWINGS_FAILURE] (state, error) {
    let status = 'Something error. Try again or contact admin!'

    let data = error.response.data

    if (data.error) {
      status = data.error.message
    }

    state.getFollowingsStatus = status
  },

  /* FOLLOWER LIST */
  [types.GET_FOLLOWERS_REQUEST] (state) {
    state.getFollowersStatus = null
  },

  [types.GET_FOLLOWERS_SUCCESS] (state, response) {
    state.getFollowersStatus = 'Successfully get  all comment likes!'

    let data = response.data

    if (state.followersCompleted) {
      return
    } else if (!data.followers) {
      state.getFollowersStatus = data.message.content
      state.followersCompleted = true
      if (data.meta) {
        state.followersCursor = Object.assign(state.followersCursor, {
          direction: data.meta.direction
        })
      }

      return
    }

    state.followers = state.followers.concat(data.followers)
    state.followersCursor = Object.assign(state.followersCursor, {
      current: data.meta.cursor.current,
      next: data.meta.cursor.next,
      prev: data.meta.cursor.prev,
      count: data.meta.cursor.count
    })

    if (state.followersCursor.count < state.followersCursor.limit) {
      state.followersCompleted = true
    }
  },

  [types.GET_FOLLOWERS_FAILURE] (state, error) {
    let status = 'Something error. Try again or contact admin!'

    let data = error.response.data

    if (data.error) {
      status = data.error.message
    }

    state.getFollowersStatus = status
  },

  /* STORE FOLLOWING */
  [types.STORE_FOLLOWING_REQUEST] (state) {
    state.storeFollowingStatus = null
  },

  [types.STORE_FOLLOWING_SUCCESS] (state, response) {
    state.storeFollowingStatus = response.data.message.content
  },

  [types.STORE_FOLLOWING_FAILURE] (state, error) {
    let status = 'Something error. Try again or contact admin!'

    let data = error.response.data

    if (data.error) {
      status = data.error.message
    }

    state.storeFollowingStatus = status
  },

  /* DELETE FOLLOWING */
  [types.DELETE_FOLLOWING_REQUEST] (state) {
    state.deleteFollowingStatus = null
  },

  [types.DELETE_FOLLOWING_SUCCESS] (state, response) {
    state.deleteFollowingStatus = response.data.message.content
  },

  [types.DELETE_FOLLOWING_FAILURE] (state, error) {
    let status = 'Something error. Try again or contact admin!'

    let data = error.response.data

    if (data.error) {
      status = data.error.message
    }

    state.deleteFollowingStatus = status
  }
}
