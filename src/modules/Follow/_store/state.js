const stateFollowingList = {
  followings: [],
  followingsCompleted: false,
  followingsCursor: {
    current: '',
    prev: '',
    next: '',
    limit: 3,
    count: 3,
    direction: ''
  },
  getFollowingsStatus: null
}

const stateFollowerList = {
  followers: [],
  followersCompleted: false,
  followersCursor: {
    current: '',
    prev: '',
    next: '',
    limit: 3,
    count: 3,
    direction: ''
  },
  getFollowersStatus: null
}

const stateStoreFollowing = {
  storeFollowingStatus: null
}

const stateDeleteFollowing = {
  deleteFollowingStatus: null
}

export default Object.assign(
  stateFollowingList,
  stateFollowerList,
  stateStoreFollowing,
  stateDeleteFollowing
)
