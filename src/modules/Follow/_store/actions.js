import * as types from './mutation-types'
import api from '../_api'
import router from '@/router'

export default {
  getFollowings ({commit, state, rootState}, direction = '') {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }

    commit(types.GET_FOLLOWINGS_REQUEST)

    let cursor = ''
    if (direction === 'newer') {
      cursor = state.cursor.prev
    } else {
      cursor = state.cursor.next
    }

    api.getFollowings(router.currentRoute.params.id, cursor, direction, { headers })
      .then((response) => {
        commit(types.GET_FOLLOWINGS_SUCCESS, response)
      })
      .catch((error) => {
        commit(types.GET_FOLLOWINGS_FAILURE, error)
      })
  },

  getFollowers ({commit, state, rootState}, direction = '') {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }

    commit(types.GET_FOLLOWERS_REQUEST)

    let cursor = ''
    if (direction === 'newer') {
      cursor = state.cursor.prev
    } else {
      cursor = state.cursor.next
    }

    api.getFollowers(router.currentRoute.params.id, cursor, direction, { headers })
      .then((response) => {
        commit(types.GET_FOLLOWERS_SUCCESS, response)
      })
      .catch((error) => {
        commit(types.GET_FOLLOWERS_FAILURE, error)
      })
  },

  storeFollowing ({commit, rootState}, userTobeFollowed) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }

    commit(types.STORE_FOLLOWING_REQUEST)

    api.storeFollowing(
      userTobeFollowed.id,
      (response) => {
        commit(types.STORE_FOLLOWING_SUCCESS, response)

        if (rootState.$_account.user && rootState.$_account.user.id === userTobeFollowed.id) {
          commit(
            'UPDATE_STATE_OBJECT',
            {module: '$_account', stateObjectKey: 'user', updatedObject: Object.assign(userTobeFollowed, {is_followed: true})},
            { root: true }
          )
        }
      },
      (error) => {
        commit(types.STORE_FOLLOWING_FAILURE, error)
      },
      { headers }
    )
  },

  deleteFollowing ({commit, rootState}, userTobeUnfollowed) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }

    commit(types.DELETE_FOLLOWING_REQUEST)

    api.deleteFollowing(
      userTobeUnfollowed.id,
      (response) => {
        commit(types.DELETE_FOLLOWING_SUCCESS, response)

        if (rootState.$_account.user && rootState.$_account.user.id === userTobeUnfollowed.id) {
          commit(
            'UPDATE_STATE_OBJECT',
            {module: '$_account', stateObjectKey: 'user', updatedObject: Object.assign(userTobeUnfollowed, {is_followed: false})},
            { root: true }
          )
        }
      },
      (error) => {
        commit(types.DELETE_FOLLOWING_FAILURE, error)
      },
      { headers }
    )
  }
}
