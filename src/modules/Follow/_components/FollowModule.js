import store from '@/store'
import followStore from '../_store'

export default {
  register () {
    store.registerModule('$_follow', followStore)
  }
}
