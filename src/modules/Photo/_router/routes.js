/* routes for photos feature */
import { loadComponent, checkAuth } from '@/utils/common'

const module = 'Photo'

export default [
  { path: '/upload',
    component: loadComponent('PhotoUpload', module),
    beforeEnter: (to, from, next) => { checkAuth(to, from, next) }
  },

  { path: '/p/:id', name: 'photo.show', component: loadComponent('PhotoShow', module) }
]
