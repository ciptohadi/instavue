import PhotoList from './_components/PhotoList'
import PhotoBrowse from './_components/PhotoBrowse'
import PhotoDetails from './_components/PhotoDetails'
import PhotoUploadForm from './_components/PhotoUploadForm'
import store from '@/store'
import photoStore from './_store'

if (!store.$_photo) store.registerModule('$_photo', photoStore)

export { PhotoList, PhotoBrowse, PhotoDetails, PhotoUploadForm }
