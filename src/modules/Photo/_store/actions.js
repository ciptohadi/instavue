import * as types from './mutation-types'
import api from '../_api'
import router from '@/router'

export default {
  getPhotos ({commit, state, rootState}) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }

    commit(types.GET_PHOTOS_REQUEST)

    setTimeout(() => {
      api.getPhotos(state.cursor.next, '', { headers })
        .then((response) => {
          commit(types.GET_PHOTOS_SUCCESS, response)
        })
        .catch((error) => {
          commit(types.GET_PHOTOS_FAILURE, error)
        })
    }, 300)
  },

  getPhotosRefresh ({commit, rootState}) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }

    commit(types.GET_PHOTOS_REFRESH_PREQUEST)
    commit(types.GET_PHOTOS_REQUEST)

    setTimeout(() => {
      api.getPhotos('', '', { headers })
        .then((response) => {
          commit(types.GET_PHOTOS_SUCCESS, response)
        })
        .catch((error) => {
          commit(types.GET_PHOTOS_FAILURE, error)
        })
    }, 300)
  },

  getPhotosForBrowse ({commit, state, rootState}) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }
    commit(types.GET_PHOTOS_BROWSE_REQUEST)

    setTimeout(() => {
      api.getPhotosForBrowse(state.browseCursor.next, '', { headers })
        .then((response) => {
          commit(types.GET_PHOTOS_BROWSE_SUCCESS, response)
        })
        .catch((error) => {
          commit(types.GET_PHOTOS_BROWSE_FAILURE, error)
        })
    }, 300)
  },

  getPhotosForBrowseRefresh ({commit, rootState}) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }
    commit(types.GET_PHOTOS_BROWSE_REFRESH_PREQUEST)
    commit(types.GET_PHOTOS_BROWSE_REQUEST)

    setTimeout(() => {
      api.getPhotosForBrowse('', '', { headers })
        .then((response) => {
          commit(types.GET_PHOTOS_BROWSE_SUCCESS, response)
        })
        .catch((error) => {
          commit(types.GET_PHOTOS_BROWSE_FAILURE, error)
        })
    }, 300)
  },

  getPhotoDetails ({ commit, rootState }, id) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }
    commit(types.GET_PHOTO_DETAILS_REQUEST)

    api.getPhotoDetails(id, { headers })
      .then((response) => {
        commit(types.GET_PHOTO_DETAILS_SUCCESS, response)
      })
      .catch((error) => {
        commit(types.GET_PHOTO_DETAILS_FAILURE, error)
      })
  },

  uploadPhoto ({ state, commit, rootState }, uploadPhotoData) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }

    commit(types.UPLOAD_PHOTO_REQUEST)

    api.uploadPhoto(
      uploadPhotoData,
      (response) => {
        commit(types.UPLOAD_PHOTO_SUCCESS, response)
        // commit(
        //   'DATA_CHANGED_SET',
        //   { photos: true, photosBrowse: true, photo: { id: response.data.photo.id } },
        //   { root: true }
        // )

        if (state.photos) {
          commit(
            'ADD_STATE_OBJECT_IN_ARRAY',
            {module: '$_photo', stateObjectArrayKey: 'photos', newObject: response.data.photo},
            { root: true }
          )
        }
        if (state.photosBrowse.length > 0) {
          commit(
            'ADD_STATE_OBJECT_IN_ARRAY',
            {module: '$_photo', stateObjectArrayKey: 'photosBrowse', newObject: response.data.photo},
            { root: true }
          )
        }

        if (rootState.$_account.user) {
          commit(
            'ADD_STATE_OBJECT_IN_ARRAY',
            {module: '$_account', stateObjectArrayKey: 'user', newObject: response.data.photo},
            { root: true }
          )
        }

        this._vm.$snotify.success(response.data.message.content)
        router.push({ name: 'photo.show', params: { id: response.data.photo.id } })
      },
      (error) => {
        commit('ERROR_CAUGHT', null, { root: true })
        commit(types.UPLOAD_PHOTO_FAILURE, error)
      },
      { headers }
    )
  },

  updatePhoto ({ state, commit, rootState }, { photoTobeUpdated, updatePhotoData }) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }
    commit(types.UPDATE_PHOTO_REQUEST)

    api.updatePhoto(
      photoTobeUpdated,
      updatePhotoData,
      (response) => {
        commit(types.UPDATE_PHOTO_SUCCESS, response)
        this._vm.$snotify.success(response.data.message.content)

        if (state.photos) {
          commit(
            'UPDATE_STATE_OBJECT_IN_ARRAY',
            {module: '$_photo', stateObjectArrayKey: 'photos', updatedObject: response.data.photo},
            { root: true }
          )
        }

        if (state.photosBrowse) {
          commit(
            'UPDATE_STATE_OBJECT_IN_ARRAY',
            {module: '$_photo', stateObjectArrayKey: 'photosBrowse', updatedObject: response.data.photo},
            { root: true }
          )
        }
        if (state.photo && state.photo.id === response.data.photo.id) {
          commit(
            'UPDATE_STATE_OBJECT',
            {module: '$_photo', stateObjectKey: 'photo', updatedObject: response.data.photo},
            { root: true }
          )
        }
      },
      (error) => {
        commit('ERROR_CAUGHT', null, { root: true })
        commit(types.UPDATE_PHOTO_FAILURE, error)
      },
      { headers }
    )
  },

  deletePhoto ({ state, commit, rootState }, photoTobeDeleted) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }
    commit(types.DELETE_PHOTO_REQUEST)

    api.deletePhoto(
      photoTobeDeleted,
      (response) => {
        commit(types.DELETE_PHOTO_SUCCESS, response)
        this._vm.$snotify.success(response.data.message.content)

        if (state.photos) {
          commit(
            'REMOVE_STATE_OBJECT_IN_ARRAY',
            {module: '$_photo', stateObjectArrayKey: 'photos', deletedObject: response.data.photo},
            { root: true }
          )
        }
        if (state.photosBrowse) {
          commit(
            'REMOVE_STATE_OBJECT_IN_ARRAY',
            {module: '$_photo', stateObjectArrayKey: 'photosBrowse', deletedObject: response.data.photo},
            { root: true }
          )
        }

        if (rootState.$_account.user) {
          commit(
            'REMOVE_STATE_OBJECT_IN_ARRAY',
            {module: '$_account', stateObjectArrayKey: 'user', deletedObject: response.data.photo},
            { root: true }
          )
        }
        if (router.currentRoute.name === 'photo.show') {
          router.push({ name: 'home' })
        }
      },
      (error) => {
        commit('ERROR_CAUGHT', null, { root: true })
        console.log(error)
        this._vm.$snotify.error(error.response.data.error.message)
        commit(types.DELETE_PHOTO_FAILURE, error)
      },
      { headers }
    )
  }
}
