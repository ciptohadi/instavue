const statePhotoList = {
  photos: [],
  completed: false,
  cursor: {
    current: '',
    prev: '',
    next: '',
    limit: 3,
    count: 3,
    direction: ''
  },
  getPhotosStatus: null
}

const statePhotoSearchList = {
  photosBrowse: [],
  browseCompleted: false,
  browseCursor: {
    current: '',
    prev: '',
    next: '',
    limit: 3,
    count: 3,
    direction: ''
  },
  getphotosBrowseStatus: null
}

const statePhotoDetails = {
  photo: null,
  getPhotoDetailsStatus: null
}

const statePhotoUpload = {
  uploadPhotoStatus: null
}

const statePhotoUpdate = {
  updatePhotoStatus: null,
  photoTobeUpdated: null
}

const statePhotoDelete = {
  deletePhotoStatus: null,
  photoTobeDeleted: null
}

const statePhotoComment = {
  commentPhotoStatus: null,
  photoTobeCommented: null
}

export default Object.assign(
  statePhotoList,
  statePhotoSearchList,
  statePhotoDetails,
  statePhotoUpload,
  statePhotoUpdate,
  statePhotoDelete,
  statePhotoComment
)
