import * as types from './mutation-types'

export default {
  /* PHOTO LIST */
  [types.GET_PHOTOS_REQUEST] (state) {
    state.getPhotosStatus = null
  },

  [types.GET_PHOTOS_SUCCESS] (state, response) {
    state.getPhotosStatus = 'Successfully get photos!'

    let data = response.data

    if (state.completed) {
      return
    } else if (!data.photos) {
      state.getPhotosStatus = data.message.content
      state.completed = true
      if (data.meta.direction) {
        state.cursor = Object.assign(state.cursor, {
          direction: data.meta.direction
        })
      }

      return
    }

    state.photos = state.photos.concat(data.photos)
    state.cursor = Object.assign(state.cursor, {
      current: data.meta.cursor.current,
      next: data.meta.cursor.next,
      prev: data.meta.cursor.prev,
      count: data.meta.cursor.count
    })

    if (state.cursor.count < state.cursor.limit) {
      state.completed = true
    }
  },

  [types.GET_PHOTOS_FAILURE] (state, error) {
    state.getPhotosStatus = 'Failed get photos!'
  },

  [types.GET_PHOTOS_REFRESH_PREQUEST] (state) {
    state.completed = false
    state.getPhotosStatus = null
    state.photos = []
  },

  /* PHOTO BROWSE LIST */
  [types.GET_PHOTOS_BROWSE_REQUEST] (state) {
    state.getPhotosSearchStatus = null
  },

  [types.GET_PHOTOS_BROWSE_SUCCESS] (state, response) {
    state.getPhotosSearchStatus = 'Successfully get photos!'

    let data = response.data

    if (state.browseCompleted) {
      return
    } else if (!data.photos) {
      state.getPhotosSearchStatus = data.message.content
      state.browseCompleted = true
      if (data.meta.direction) {
        state.browseCursor = Object.assign(state.browseCursor, {
          direction: data.meta.direction
        })
      }

      return
    }

    state.photosBrowse = state.photosBrowse.concat(data.photos)
    state.browseCursor = Object.assign(state.browseCursor, {
      current: data.meta.cursor.current,
      next: data.meta.cursor.next,
      prev: data.meta.cursor.prev,
      count: data.meta.cursor.count
    })

    if (state.browseCursor.count < state.browseCursor.limit) {
      state.browseCompleted = true
    }
  },

  [types.GET_PHOTOS_BROWSE_FAILURE] (state, error) {
    state.getPhotosSearchStatus = 'Failed get photos!'
  },

  [types.GET_PHOTOS_BROWSE_REFRESH_PREQUEST] (state) {
    state.browseCompleted = false
    state.getPhotosSearchStatus = null
    state.photosBrowse = []
  },

  /* PHOTO DETAILS */
  [types.GET_PHOTO_DETAILS_REQUEST] (state) {
    state.getPhotoDetailsStatus = null
    state.photo = null
  },

  [types.GET_PHOTO_DETAILS_SUCCESS] (state, response) {
    state.getPhotoDetailsStatus = 'Successfully get photo details!'
    state.photo = response.data.photo
  },

  [types.GET_PHOTO_DETAILS_FAILURE] (state, error) {
    state.getPhotoDetailstatus = 'Failed get photo details!'
  },

  /* PHOTO UPLOAD */
  [types.UPLOAD_PHOTO_REQUEST] (state) {
    state.uploadPhotoStatus = null
  },

  [types.UPLOAD_PHOTO_SUCCESS] (state, response) {
    state.uploadPhotoStatus = response.data.message.content
  },

  [types.UPLOAD_PHOTO_FAILURE] (state, error) {
    let status = 'Something error. Try again or contact admin!'

    let data = error.response.data

    if (data.error) {
      status = data.error.message
    } else if (data.meta.message === 'VALIDATION_FAILED') {
      // validation errors
      status = data.errors
    }

    state.uploadPhotoStatus = status
  },

  /* PHOTO UPDATE */
  [types.UPDATE_PHOTO_REQUEST] (state) {
    state.updatePhotoStatus = null
  },

  [types.UPDATE_PHOTO_SUCCESS] (state, response) {
    state.updatePhotoStatus = response.data.message.content
  },

  [types.UPDATE_PHOTO_FAILURE] (state, error) {
    let status = 'Something error. Try again or contact admin!'

    let data = error.response.data

    if (data.error) {
      status = data.error.message
    } else if (data.meta.message === 'VALIDATION_FAILED') {
      // validation errors
      status = data.errors
    }

    state.updatePhotoStatus = status
  },

  [types.SET_PHOTO_TOBE_UPDATED] (state, photo) {
    state.photoTobeUpdated = photo
  },

  /* PHOTO DELETE */
  [types.DELETE_PHOTO_REQUEST] (state) {
    state.deletePhotoStatus = null
  },

  [types.DELETE_PHOTO_SUCCESS] (state, response) {
    state.deletePhotoStatus = response.data.message.content
  },

  [types.DELETE_PHOTO_FAILURE] (state, error) {
    state.deletePhotoStatus = error.response.data.error.message
  },

  [types.SET_PHOTO_TOBE_DELETED] (state, photo) {
    state.photoTobeDeleted = photo
  },

  [types.SET_PHOTO_TOBE_COMMENTED] (state, photo) {
    state.photoTobeCommented = photo
  },

  [types.RESET_PHOTO_DATA] (state) {
    state.photos = []
    state.completed = false
    state.cursor = {
      current: '',
      prev: '',
      next: '',
      limit: 3,
      count: 3,
      direction: ''
    }
    state.photosBrowse = []
    state.browseCompleted = false
    state.browseCursor = {
      current: '',
      prev: '',
      next: '',
      limit: 3,
      count: 3,
      direction: ''
    }
    state.photo = null
  }
}
