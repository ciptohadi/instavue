const photos = state => state.photos
const photosBrowse = state => state.photosBrowse
const photo = state => state.photo
const completed = state => state.completed
const browseCompleted = state => state.browseCompleted
const cursorDirection = state => state.cursor.direction

const getPhotosStatus = state => state.getPhotosStatus
const getPhotoDetailsStatus = state => state.getPhotoDetailsStatus
const uploadPhotoStatus = state => state.uploadPhotoStatus
const updatePhotoStatus = state => state.updatePhotoStatus
const deletePhotoStatus = state => state.deletePhotoStatus
const photoTobeUpdated = state => state.photoTobeUpdated
const photoTobeDeleted = state => state.photoTobeDeleted
const photoTobeCommented = state => state.photoTobeCommented

export default {
  photos,
  photosBrowse,
  photo,
  completed,
  browseCompleted,
  getPhotosStatus,
  getPhotoDetailsStatus,
  uploadPhotoStatus,
  updatePhotoStatus,
  deletePhotoStatus,
  photoTobeUpdated,
  photoTobeDeleted,
  photoTobeCommented,
  cursorDirection
}
