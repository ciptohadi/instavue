import { HTTP } from '@/utils/http-common'

const getPhotos = (cursor = '', direction = '', config = {}) => (
  HTTP.get(`account/me/photos-timeline?cursor=${cursor}&direction=${direction}`, config)
    .then((response) => { return response })
    .catch((error) => { throw error })
)

const getPhotosForBrowse = (cursor = '', direction = '', config = {}) => (
  HTTP.get(`photos?cursor=${cursor}&direction=${direction}`, config)
    .then((response) => { return response })
    .catch((error) => { throw error })
)

const getPhotoDetails = (id, config = {}) => (
  HTTP.get(`photos/${id}`, config)
    .then((response) => { return response })
    .catch((error) => { throw error })
)

const uploadPhoto = (uploadPhotoData, cb, errCb, config = {}) => {
  HTTP.post(`photos`, uploadPhotoData, config)
  .then(response => {
    cb(response)
  })
  .catch(error => {
    errCb(error)
  })
}

const updatePhoto = (photoTobeUpdated, updatePhotoData, cb, errCb, config = {}) => {
  HTTP.post(`photos/${photoTobeUpdated.id}`, updatePhotoData, config)
  .then(response => {
    cb(response)
  })
  .catch(error => {
    errCb(error)
  })
}

const deletePhoto = (photoTobeDeleted, cb, errCb, config = {}) => {
  HTTP.delete(`photos/${photoTobeDeleted.id}`, config)
  .then(response => {
    cb(response)
  })
  .catch(error => {
    errCb(error)
  })
}

export default {
  getPhotos, getPhotosForBrowse, getPhotoDetails, uploadPhoto, updatePhoto, deletePhoto
}
