import { HTTP } from '@/utils/http-common'

const getComments = (id, cursor = '', direction = '', focus = 'no', config = {}) => (
  HTTP.get(`photos/${id}/comments?cursor=${cursor}&direction=${direction}&focus=${focus}`, config)
    .then((response) => { return response })
    .catch((error) => { throw error })
)

const getChildComments = (pid, cid, cursor = '', direction = '', focus = 'no', config = {}) => (
  HTTP.get(`photos/${pid}/comments/${cid}/child-comments?cursor=${cursor}&direction=${direction}&focus=${focus}`, config)
    .then((response) => { return response })
    .catch((error) => { throw error })
)

const submitPhotoComment = (id, submitCommentData, cb, errCb, config = {}) => {
  HTTP.post(`photos/${id}/comments`, submitCommentData, config)
  .then(response => {
    cb(response)
  })
  .catch(error => {
    errCb(error)
  })
}

const submitChildComment = (pid, cid, submitChildCommentData, cb, errCb, config = {}) => {
  HTTP.post(`photos/${pid}/comments/${cid}/comments`, submitChildCommentData, config)
  .then(response => {
    cb(response)
  })
  .catch(error => {
    errCb(error)
  })
}

const updateComment = (id, commentTobeUpdated, updateCommentData, cb, errCb, config = {}) => {
  HTTP.post(`photos/${id}/comments/${commentTobeUpdated.id}`, updateCommentData, config)
  .then(response => {
    cb(response)
  })
  .catch(error => {
    errCb(error)
  })
}

const deleteComment = (id, commentTobeDeleted, cb, errCb, config = {}) => {
  HTTP.delete(`photos/${id}/comments/${commentTobeDeleted.id}`, config)
  .then(response => {
    cb(response)
  })
  .catch(error => {
    errCb(error)
  })
}

export default {
  getComments, getChildComments, submitPhotoComment, submitChildComment, updateComment, deleteComment
}
