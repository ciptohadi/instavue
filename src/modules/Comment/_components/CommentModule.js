import store from '@/store'
import commentStore from '../_store'

export default {
  register () {
    store.registerModule('$_comment', commentStore)
  }
}
