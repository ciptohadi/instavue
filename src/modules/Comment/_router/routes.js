/* routes for photos feature */
import { loadComponent, checkAuth } from '@/utils/common'

const module = 'Comment'

export default [
  {
    path: '/p/:id/c',
    name: 'photo.comment.index',
    component: loadComponent('PhotoCommentIndex', module),
    beforeEnter: (to, from, next) => { checkAuth(to, from, next) }
  },

  {
    path: '/p/:pid/c/:cid/cc',
    name: 'photo.comment.childcomment.index',
    component: loadComponent('PhotoCommentChildcommentIndex', module),
    beforeEnter: (to, from, next) => { checkAuth(to, from, next) }
  }
]
