const stateCommentList = {
  comments: [],
  completed: false,
  cursor: {
    current: '',
    prev: '',
    next: '',
    limit: 3,
    count: 3,
    direction: ''
  },
  getCommentsStatus: null
}

const stateChildCommentList = {
  childComments: [],
  childCompleted: false,
  childCursor: {
    current: '',
    prev: '',
    next: '',
    limit: 3,
    count: 3,
    direction: ''
  },
  getChildCommentsStatus: null
}

const stateCommentSubmit = {
  submitCommentStatus: null
}

const stateCommentUpdate = {
  updateCommentStatus: null,
  commentTobeUpdated: null
}

const stateCommentDelete = {
  deleteCommentStatus: null,
  commentTobeDeleted: null
}

const stateChildComment = {
  childCommentStatus: null,
  commentTobeReplied: null
}

export default Object.assign(
  stateCommentList,
  stateChildCommentList,
  stateCommentSubmit,
  stateCommentUpdate,
  stateCommentDelete,
  stateChildComment
)
