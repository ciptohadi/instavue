import * as types from './mutation-types'

export default {
  /* COMMENT LIST */
  [types.GET_COMMENTS_REQUEST] (state) {
    state.getCommentsStatus = null
  },

  [types.GET_COMMENTS_SUCCESS] (state, response) {
    state.getCommentsStatus = 'Successfully get  all comments!'

    let data = response.data

    if (state.completed) {
      return
    } else if (!data.comments) {
      state.getCommentsStatus = data.message.content
      state.completed = true
      if (data.meta) {
        state.cursor = Object.assign(state.cursor, {
          direction: data.meta.direction
        })
      }

      return
    }

    state.comments = state.comments.concat(data.comments)
    state.cursor = Object.assign(state.cursor, {
      current: data.meta.cursor.current,
      next: data.meta.cursor.next,
      prev: data.meta.cursor.prev,
      count: data.meta.cursor.count
    })

    if (state.cursor.count < state.cursor.limit) {
      state.completed = true
    }
  },

  [types.GET_COMMENTS_FAILURE] (state, error) {
    let status = 'Something error. Try again or contact admin!'
    console.log(error)
    let data = error.response.data

    if (data.error) {
      status = data.error.message
    }

    state.getCommentsStatus = status
  },

  [types.RESET_COMMENTS] (state) {
    state.comments = []
    state.completed = false
    state.cursor = Object.assign(state.cursor, {
      current: '',
      prev: '',
      next: '',
      limit: 3
    })
    state.getCommentsStatus = null
  },

  /* CHILD COMMENT LIST */
  [types.GET_CHILD_COMMENTS_REQUEST] (state) {
    state.getChildCommentsStatus = null
  },

  [types.GET_CHILD_COMMENTS_SUCCESS] (state, response) {
    state.getChildCommentsStatus = 'Successfully get all comments!'

    let data = response.data

    if (state.childCompleted) {
      return
    } else if (!data.comments) {
      state.getChildCommentsStatus = data.message.content
      state.childCompleted = true
      if (data.meta) {
        state.childCursor = Object.assign(state.childCursor, {
          direction: data.meta.direction
        })
      }

      return
    }

    state.childComments = state.childComments.concat(data.comments)
    state.childCursor = Object.assign(state.childCursor, {
      current: data.meta.cursor.current,
      next: data.meta.cursor.next,
      prev: data.meta.cursor.prev,
      count: data.meta.cursor.count
    })

    if (state.childCursor.count < state.childCursor.limit) {
      state.childCompleted = true
    }
  },

  [types.GET_CHILD_COMMENTS_FAILURE] (state, error) {
    let status = 'Something error. Try again or contact admin!'
    let data = error.response.data

    if (data.error) {
      status = data.error.message
    }

    state.getChildCommentsStatus = status
  },

  [types.RESET_CHILD_COMMENTS] (state) {
    state.childComments = []
    state.childCompleted = false
    state.childCursor = Object.assign(state.cursor, {
      current: '',
      prev: '',
      next: '',
      limit: 3
    })
    state.getChildCommentsStatus = null
  },

  /* COMMENT SUBMIT */
  [types.SUBMIT_COMMENT_REQUEST] (state) {
    state.submitCommentStatus = null
  },

  [types.SUBMIT_COMMENT_SUCCESS] (state, response) {
    state.submitCommentStatus = response.data.message.content
  },

  [types.SUBMIT_COMMENT_FAILURE] (state, error) {
    let status = 'Something error. Try again or contact admin!'

    let data = error.response.data

    if (data.error) {
      status = data.error.message
    } else if (data.meta.message === 'VALIDATION_FAILED') {
      // validation errors
      status = data.errors
    }

    state.submitCommentStatus = status
  },

  /* CHILD COMMENT SUBMIT */
  [types.SUBMIT_CHILD_COMMENT_REQUEST] (state) {
    state.submitCommentStatus = null
  },

  [types.SUBMIT_CHILD_COMMENT_SUCCESS] (state, response) {
    state.submitCommentStatus = response.data.message.content
  },

  [types.SUBMIT_CHILD_COMMENT_FAILURE] (state, error) {
    let status = 'Something error. Try again or contact admin!'

    let data = error.response.data

    if (data.error) {
      status = data.error.message
    } else if (data.meta.message === 'VALIDATION_FAILED') {
      // validation errors
      status = data.errors
    }

    state.submitCommentStatus = status
  },

  /* COMMENT UPDATE */
  [types.UPDATE_COMMENT_REQUEST] (state) {
    state.updateCommentStatus = null
  },

  [types.UPDATE_COMMENT_SUCCESS] (state, response) {
    state.updateCommentStatus = response.data.message.content
  },

  [types.UPDATE_COMMENT_FAILURE] (state, error) {
    let status = 'Something error. Try again or contact admin!'
    console.log(error)
    let data = error.response.data

    if (data.error) {
      status = data.error.message
    } else if (data.meta.message === 'VALIDATION_FAILED') {
      // validation errors
      status = data.errors
    }

    state.updateCommentStatus = status
  },

  [types.SET_COMMENT_TOBE_UPDATED] (state, comment) {
    state.commentTobeUpdated = comment
  },

  /* COMMENT DELETE */
  [types.DELETE_COMMENT_REQUEST] (state) {
    state.deleteCommentStatus = null
  },

  [types.DELETE_COMMENT_SUCCESS] (state, response) {
    state.deleteCommentStatus = response.data.message.content
  },

  [types.DELETE_COMMENT_FAILURE] (state, error) {
    let status = 'Something error. Try again or contact admin!'
    let data = error.response.data

    if (data.error) {
      status = data.error.message
    }

    state.deleteCommentStatus = status
  },

  [types.SET_COMMENT_TOBE_DELETED] (state, comment) {
    state.commentTobeDeleted = comment
  },

  [types.SET_COMMENT_TOBE_REPLIED] (state, comment) {
    state.commentTobeReplied = comment
  }
}
