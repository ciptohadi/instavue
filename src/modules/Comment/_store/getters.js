const comments = state => state.comments
const childComments = state => state.childComments
const completed = state => state.completed
const childCompleted = state => state.childCompleted
const cursorDirection = state => state.cursor.direction
const childCursorDirection = state => state.childCursor.direction
const commentsCount = state => state.cursor.count
const childCommentsCount = state => state.childCursor.count

const getCommentsStatus = state => state.getCommentsStatus
const getChildCommentsStatus = state => state.getChildCommentsStatus
const submitCommentStatus = state => state.submitCommentStatus
const updateCommentStatus = state => state.updateCommentStatus
const deleteCommentStatus = state => state.deleteCommentStatus
const commentTobeUpdated = state => state.commentTobeUpdated
const commentTobeDeleted = state => state.commentTobeDeleted
const commentTobeReplied = state => state.commentTobeReplied

export default {
  comments,
  childComments,
  completed,
  childCompleted,
  getCommentsStatus,
  getChildCommentsStatus,
  submitCommentStatus,
  updateCommentStatus,
  deleteCommentStatus,
  commentTobeUpdated,
  commentTobeDeleted,
  cursorDirection,
  childCursorDirection,
  commentsCount,
  childCommentsCount,
  commentTobeReplied
}
