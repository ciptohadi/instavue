import * as types from './mutation-types'
import api from '../_api'
import router from '@/router'

export default {
  getComments ({commit, state, rootState}, direction = '') {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }

    commit(types.GET_COMMENTS_REQUEST)

    let cursor = ''
    let focus = ''

    if (direction === 'newer') {
      cursor = state.cursor.prev
    } else {
      cursor = state.cursor.next
    }

    if (!cursor) {
      cursor = router.currentRoute.query.cursor
      focus = router.currentRoute.query.focus
    }

    let pid = router.currentRoute.params.id
    api.getComments(pid, cursor, direction, focus, { headers })
      .then((response) => {
        commit(types.GET_COMMENTS_SUCCESS, response)
      })
      .catch((error) => {
        commit(types.GET_COMMENTS_FAILURE, error)
      })
  },

  getChildComments ({commit, state, rootState}, direction = '') {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }

    commit(types.GET_CHILD_COMMENTS_REQUEST)

    let cursor = ''
    let focus = ''

    if (direction === 'newer') {
      cursor = state.childCursor.prev
    } else {
      cursor = state.childCursor.next
    }

    if (!cursor) {
      cursor = router.currentRoute.query.cursor
      focus = router.currentRoute.query.focus
    }

    let pid = router.currentRoute.params.pid
    let cid = router.currentRoute.params.cid
    api.getChildComments(pid, cid, cursor, direction, focus, { headers })
      .then((response) => {
        commit(types.GET_CHILD_COMMENTS_SUCCESS, response)
      })
      .catch((error) => {
        commit(types.GET_CHILD_COMMENTS_FAILURE, error)
      })
  },

  submitPhotoComment ({state, commit, dispatch, rootState}, submitCommentData) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }

    commit(types.SUBMIT_COMMENT_REQUEST)

    api.submitPhotoComment(
      rootState.$_photo.photoTobeCommented.id,
      submitCommentData,
      (response) => {
        commit(types.SUBMIT_COMMENT_SUCCESS, response)
        this._vm.$snotify.success(response.data.message.content)
        commit(types.RESET_COMMENTS)
        dispatch('getComments')
      },
      (error) => {
        commit(types.SUBMIT_COMMENT_FAILURE, error)
        this._vm.$snotify.error(error.response.data.error.message)
      },
      { headers }
    )
  },

  submitChildComment ({state, commit, dispatch, rootState}, submitCommentData) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }

    commit(types.SUBMIT_CHILD_COMMENT_REQUEST)

    api.submitChildComment(
      rootState.$_photo.photoTobeCommented.id,
      state.commentTobeReplied.id,
      submitCommentData,
      (response) => {
        commit(types.SUBMIT_CHILD_COMMENT_SUCCESS, response)
        this._vm.$snotify.success(response.data.message.content)
        commit(types.RESET_CHILD_COMMENTS)
        dispatch('getChildComments')
      },
      (error) => {
        commit(types.SUBMIT_CHILD_COMMENT_FAILURE, error)
        this._vm.$snotify.error(error.response.data.error.message)
      },
      { headers }
    )
  },

  updateComment ({ commit, dispatch, rootState }, { commentTobeUpdated, updateCommentData }) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }
    commit(types.UPDATE_COMMENT_REQUEST)

    api.updateComment(
      rootState.$_photo.photoTobeCommented.id,
      commentTobeUpdated,
      updateCommentData,
      (response) => {
        commit(types.UPDATE_COMMENT_SUCCESS, response)
        this._vm.$snotify.success(response.data.message.content)
        if (router.currentRoute.name === 'photo.comment.index') {
          commit(types.RESET_COMMENTS)
          dispatch('getComments')
        } else {
          commit(types.RESET_CHILD_COMMENTS)
          dispatch('getChildComments')
        }
      },
      (error) => {
        commit(types.UPDATE_COMMENT_FAILURE, error)
        this._vm.$snotify.error(error.response.data.error.message)
      },
      { headers }
    )
  },

  deleteComment ({ commit, dispatch, rootState }, commentTobeDeleted) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }
    commit(types.DELETE_COMMENT_REQUEST)

    api.deleteComment(
      rootState.$_photo.photoTobeCommented.id,
      commentTobeDeleted,
      (response) => {
        commit(types.DELETE_COMMENT_SUCCESS, response)
        this._vm.$snotify.success(response.data.message.content)

        if (router.currentRoute.name === 'photo.comment.index') {
          commit(types.RESET_COMMENTS)
          dispatch('getComments')
        } else {
          commit(types.RESET_CHILD_COMMENTS)
          dispatch('getChildComments')
        }
      },
      (error) => {
        commit(types.DELETE_COMMENT_FAILURE, error)
        this._vm.$snotify.error(error.response.data.error.message)
      },
      { headers }
    )
  }
}
