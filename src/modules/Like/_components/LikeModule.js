import store from '@/store'
import likeStore from '../_store'

export default {
  register () {
    store.registerModule('$_like', likeStore)
  }
}
