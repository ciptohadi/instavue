const photoLikes = state => state.photoLikes
const photoLikesCompleted = state => state.photoLikesCompleted
const photoLikesCursor = state => state.photoLikesCursor
const getPhotoLikesStatus = state => state.getPhotoLikesStatus

const commentLikes = state => state.commentLikes
const commentLikesCompleted = state => state.commentLikesCompleted
const commentLikesCursor = state => state.commentLikesCursor
const getCommentLikesStatus = state => state.getCommentLikesStatus

const storePhotoLikeStatus = state => state.storePhotoLikeStatus
const storeCommentLikeStatus = state => state.storeCommentLikeStatus
const deletePhotoLikeStatus = state => state.deletePhotoLikeStatus
const deleteCommentLikeStatus = state => state.deleteCommentLikeStatus

export default {
  photoLikes,
  photoLikesCompleted,
  photoLikesCursor,
  getPhotoLikesStatus,
  commentLikes,
  commentLikesCompleted,
  commentLikesCursor,
  getCommentLikesStatus,
  storePhotoLikeStatus,
  storeCommentLikeStatus,
  deletePhotoLikeStatus,
  deleteCommentLikeStatus
}
