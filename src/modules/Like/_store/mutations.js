import * as types from './mutation-types'

export default {
  /* PHOTO LIKES LIST */
  [types.GET_PHOTO_LIKES_REQUEST] (state) {
    state.getPhotoLikesStatus = null
  },

  [types.GET_PHOTO_LIKES_SUCCESS] (state, response) {
    state.getPhotoLikesStatus = 'Successfully get  all photo likes!'

    let data = response.data

    if (state.photoLikesCompleted) {
      return
    } else if (!data.likes) {
      state.getPhotoLikesStatus = data.message.content
      state.photoLikesCompleted = true
      if (data.meta) {
        state.photoLikesCursor = Object.assign(state.photoLikesCursor, {
          direction: data.meta.direction
        })
      }

      return
    }

    state.photoLikes = state.photoLikes.concat(data.likes)
    state.photoLikesCursor = Object.assign(state.photoLikesCursor, {
      current: data.meta.cursor.current,
      next: data.meta.cursor.next,
      prev: data.meta.cursor.prev,
      count: data.meta.cursor.count
    })

    if (state.photoLikesCursor.count < state.photoLikesCursor.limit) {
      state.photoLikesCompleted = true
    }
  },

  [types.GET_PHOTO_LIKES_FAILURE] (state, error) {
    let status = 'Something error. Try again or contact admin!'

    let data = error.response.data

    if (data.error) {
      status = data.error.message
    }

    state.getPhotoLikesStatus = status
  },

  /* COMMENT LIKES LIST */
  [types.GET_COMMENT_LIKES_REQUEST] (state) {
    state.getCommentLikesStatus = null
  },

  [types.GET_COMMENT_LIKES_SUCCESS] (state, response) {
    state.getCommentLikesStatus = 'Successfully get  all comment likes!'

    let data = response.data

    if (state.commentLikesCompleted) {
      return
    } else if (!data.likes) {
      state.getCommentLikesStatus = data.message.content
      state.commentLikesCompleted = true
      if (data.meta) {
        state.commentLikesCursor = Object.assign(state.commentLikesCursor, {
          direction: data.meta.direction
        })
      }

      return
    }

    state.commentLikes = state.commentLikes.concat(data.likes)
    state.commentLikesCursor = Object.assign(state.commentLikesCursor, {
      current: data.meta.cursor.current,
      next: data.meta.cursor.next,
      prev: data.meta.cursor.prev,
      count: data.meta.cursor.count
    })

    if (state.commentLikesCursor.count < state.commentLikesCursor.limit) {
      state.commentLikesCompleted = true
    }
  },

  [types.GET_COMMENT_LIKES_FAILURE] (state, error) {
    let status = 'Something error. Try again or contact admin!'

    let data = error.response.data

    if (data.error) {
      status = data.error.message
    }

    state.getCommentLikesStatus = status
  },

  /* STORE PHOTO LIKE */
  [types.STORE_PHOTO_LIKE_REQUEST] (state) {
    state.storePhotoLikeStatus = null
  },

  [types.STORE_PHOTO_LIKE_SUCCESS] (state, response) {
    state.storePhotoLikeStatus = response.data.message.content
  },

  [types.STORE_PHOTO_LIKE_FAILURE] (state, error) {
    let status = 'Something error. Try again or contact admin!'
    console.log(error)
    let data = error.response.data

    if (data.error) {
      status = data.error.message
    }

    state.storePhotoLikeStatus = status
  },

  /* STORE COMMENT LIKE */
  [types.STORE_COMMENT_LIKE_REQUEST] (state) {
    state.storeCommentLikeStatus = null
  },

  [types.STORE_COMMENT_LIKE_SUCCESS] (state, response) {
    state.storeCommentLikeStatus = response.data.message.content
  },

  [types.STORE_COMMENT_LIKE_FAILURE] (state, error) {
    let status = 'Something error. Try again or contact admin!'
    console.log(error)
    let data = error.response.data

    if (data.error) {
      status = data.error.message
    }

    state.storeCommentLikeStatus = status
  },

  /* DELETE PHOTO LIKE */
  [types.DELETE_PHOTO_LIKE_REQUEST] (state) {
    state.deletePhotoLikeStatus = null
  },

  [types.DELETE_PHOTO_LIKE_SUCCESS] (state, response) {
    state.deletePhotoLikeStatus = response.data.message.content
  },

  [types.DELETE_PHOTO_LIKE_FAILURE] (state, error) {
    let status = 'Something error. Try again or contact admin!'

    let data = error.response.data

    if (data.error) {
      status = data.error.message
    }

    state.deletePhotoLikeStatus = status
  },

  /* DELETE COMMENT LIKE */
  [types.DELETE_COMMENT_LIKE_REQUEST] (state) {
    state.deleteCommentLikeStatus = null
  },

  [types.DELETE_COMMENT_LIKE_SUCCESS] (state, response) {
    state.deleteCommentLikeStatus = response.data.message.content
  },

  [types.DELETE_COMMENT_LIKE_FAILURE] (state, error) {
    let status = 'Something error. Try again or contact admin!'

    let data = error.response.data

    if (data.error) {
      status = data.error.message
    }

    state.deleteCommentLikeStatus = status
  }
}
