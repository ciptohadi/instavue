import * as types from './mutation-types'
import api from '../_api'
import router from '@/router'

export default {
  getPhotoLikes ({commit, state, rootState}, direction = '') {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }

    commit(types.GET_PHOTO_LIKES_REQUEST)

    let cursor = ''
    if (direction === 'newer') {
      cursor = state.cursor.prev
    } else {
      cursor = state.cursor.next
    }

    api.getPhotoLikes(router.currentRoute.params.id, cursor, direction, { headers })
      .then((response) => {
        commit(types.GET_PHOTO_LIKES_SUCCESS, response)
      })
      .catch((error) => {
        commit(types.GET_PHOTO_LIKES_FAILURE, error)
      })
  },

  getCommentLikes ({commit, state, rootState}, direction = '') {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }

    commit(types.GET_COMMENT_LIKES_REQUEST)

    let cursor = ''
    if (direction === 'newer') {
      cursor = state.childCursor.prev
    } else {
      cursor = state.childCursor.next
    }

    api.getCommentLikes(router.currentRoute.params.pid, router.currentRoute.params.cid, cursor, direction, { headers })
      .then((response) => {
        commit(types.GET_COMMENT_LIKES_SUCCESS, response)
      })
      .catch((error) => {
        commit(types.GET_COMMENT_LIKES_FAILURE, error)
      })
  },

  storePhotoLike ({state, commit, rootState}, photoTobeLiked) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }

    commit(types.STORE_PHOTO_LIKE_REQUEST)

    api.storePhotoLike(
      photoTobeLiked.id,
      (response) => {
        commit(types.STORE_PHOTO_LIKE_SUCCESS, response)

        let updatedObject = Object.assign(photoTobeLiked,
          {
            liked: {is: true, id: response.data.like.id},
            likes: {count: ++photoTobeLiked.likes.count}
          })

        if (rootState.$_photo.photos) {
          commit(
            'UPDATE_STATE_OBJECT_IN_ARRAY',
            {module: '$_photo', stateObjectArrayKey: 'photos', updatedObject: updatedObject},
            { root: true }
          )
        }

        if (rootState.$_photo.photosSearch) {
          commit(
            'UPDATE_STATE_OBJECT_IN_ARRAY',
            {module: '$_photo', stateObjectArrayKey: 'photosSearch', updatedObject: updatedObject},
            { root: true }
          )
        }

        if (rootState.$_photo.photo && rootState.$_photo.photo.id === photoTobeLiked.id) {
          commit(
            'UPDATE_STATE_OBJECT',
            {module: '$_photo', stateObjectKey: 'photo', updatedObject: updatedObject},
            { root: true }
          )
        }
      },
      (error) => {
        commit(types.STORE_PHOTO_LIKE_FAILURE, error)
      },
      { headers }
    )
  },

  storeCommentLike ({state, commit, rootState}, {pid, commentTobeLiked}) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }

    commit(types.STORE_COMMENT_LIKE_REQUEST)

    api.storeCommentLike(
      pid,
      commentTobeLiked.id,
      (response) => {
        commit(types.STORE_COMMENT_LIKE_SUCCESS, response)

        let updatedObject = Object.assign(commentTobeLiked,
          {
            liked: {is: true, id: response.data.like.id},
            likes: {count: ++commentTobeLiked.likes.count}
          })
        if (rootState.$_comment.comments) {
          commit(
            'UPDATE_STATE_OBJECT_IN_ARRAY',
            {module: '$_comment', stateObjectArrayKey: 'comments', updatedObject: updatedObject},
            { root: true }
          )
        }

        if (rootState.$_comment.childComments) {
          commit(
            'UPDATE_STATE_OBJECT_IN_ARRAY',
            {module: '$_comment', stateObjectArrayKey: 'childComments', updatedObject: updatedObject},
            { root: true }
          )
        }
      },
      (error) => {
        commit(types.STORE_COMMENT_LIKE_FAILURE, error)
      },
      { headers }
    )
  },

  deletePhotoLike ({state, commit, rootState}, photoTobeUnliked) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }

    commit(types.DELETE_PHOTO_LIKE_REQUEST)

    api.deletePhotoLike(
      photoTobeUnliked.id,
      photoTobeUnliked.liked.id,
      (response) => {
        commit(types.DELETE_PHOTO_LIKE_SUCCESS, response)

        let updatedObject = Object.assign(photoTobeUnliked,
          {
            liked: {is: false, id: null},
            likes: {count: --photoTobeUnliked.likes.count}
          })

        if (rootState.$_photo.photos) {
          commit(
            'UPDATE_STATE_OBJECT_IN_ARRAY',
            {module: '$_photo', stateObjectArrayKey: 'photos', updatedObject: updatedObject},
            { root: true }
          )
        }

        if (rootState.$_photo.photosSearch) {
          commit(
            'UPDATE_STATE_OBJECT_IN_ARRAY',
            {module: '$_photo', stateObjectArrayKey: 'photosSearch', updatedObject: updatedObject},
            { root: true }
          )
        }
        if (rootState.$_photo.photo && rootState.$_photo.photo.id === photoTobeUnliked.id) {
          commit(
            'UPDATE_STATE_OBJECT',
            {module: '$_photo', stateObjectKey: 'photo', updatedObject: updatedObject},
            { root: true }
          )
        }
      },
      (error) => {
        commit(types.DELETE_PHOTO_LIKE_FAILURE, error)
      },
      { headers }
    )
  },

  deleteCommentLike ({state, commit, rootState}, {pid, commentTobeUnliked}) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }

    commit(types.DELETE_COMMENT_LIKE_REQUEST)

    api.deleteCommentLike(
      pid,
      commentTobeUnliked.id,
      commentTobeUnliked.liked.id,
      (response) => {
        commit(types.DELETE_COMMENT_LIKE_SUCCESS, response)

        let updatedObject = Object.assign(commentTobeUnliked,
          {
            liked: {is: false, id: null},
            likes: {count: --commentTobeUnliked.likes.count}
          })

        if (rootState.$_comment.comments) {
          commit(
            'UPDATE_STATE_OBJECT_IN_ARRAY',
            {module: '$_comment', stateObjectArrayKey: 'comments', updatedObject: updatedObject},
            { root: true }
          )
        }

        if (rootState.$_comment.childComments) {
          commit(
            'UPDATE_STATE_OBJECT_IN_ARRAY',
            {module: '$_comment', stateObjectArrayKey: 'childComments', updatedObject: updatedObject},
            { root: true }
          )
        }
      },
      (error) => {
        commit(types.DELETE_COMMENT_LIKE_FAILURE, error)
      },
      { headers }
    )
  }
}
