const statePhotoLikesList = {
  photoLikes: [],
  photoLikesCompleted: false,
  photoLikesCursor: {
    current: '',
    prev: '',
    next: '',
    limit: 3,
    count: 3,
    direction: ''
  },
  getPhotoLikesStatus: null
}

const stateCommentLikesList = {
  commentLikes: [],
  commentLikesCompleted: false,
  commentLikesCursor: {
    current: '',
    prev: '',
    next: '',
    limit: 3,
    count: 3,
    direction: ''
  },
  getCommentLikesStatus: null
}

const stateStorePhotoLike = {
  storePhotoLikeStatus: null
}

const stateStoreCommentLike = {
  storeCommentLikeStatus: null
}

const stateDeletePhotoLike = {
  deletePhotoLikeStatus: null
}

const stateDeleteCommentLike = {
  deleteCommentLikeStatus: null
}

export default Object.assign(
  statePhotoLikesList,
  stateCommentLikesList,
  stateStorePhotoLike,
  stateStoreCommentLike,
  stateDeletePhotoLike,
  stateDeleteCommentLike
)
