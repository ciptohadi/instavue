import { HTTP } from '@/utils/http-common'

const getPhotoLikes = (id, cursor = '', direction = '', config = {}) => (
  HTTP.get(`photos/${id}/likes?cursor=${cursor}&direction=${direction}`, config)
    .then((response) => { return response })
    .catch((error) => { throw error })
)

const getCommentLikes = (pid, cid, cursor = '', direction = '', config = {}) => (
  HTTP.get(`photos/${pid}/comments/${cid}/likes?cursor=${cursor}&direction=${direction}`, config)
    .then((response) => { return response })
    .catch((error) => { throw error })
)

const storePhotoLike = (id, cb, errCb, config = {}) => {
  HTTP.post(`photos/${id}/likes`, {}, config)
  .then(response => {
    cb(response)
  })
  .catch(error => {
    errCb(error)
  })
}

const storeCommentLike = (pid, cid, cb, errCb, config = {}) => {
  HTTP.post(`photos/${pid}/comments/${cid}/likes`, {}, config)
  .then(response => {
    cb(response)
  })
  .catch(error => {
    errCb(error)
  })
}

const deletePhotoLike = (pid, lid, cb, errCb, config = {}) => {
  HTTP.delete(`photos/${pid}/likes/${lid}`, config)
  .then(response => {
    cb(response)
  })
  .catch(error => {
    errCb(error)
  })
}

const deleteCommentLike = (pid, cid, lid, cb, errCb, config = {}) => {
  HTTP.delete(`photos/${pid}/comments/${cid}/likes/${lid}`, config)
  .then(response => {
    cb(response)
  })
  .catch(error => {
    errCb(error)
  })
}

export default {
  getPhotoLikes, getCommentLikes, storePhotoLike, storeCommentLike, deletePhotoLike, deleteCommentLike
}
