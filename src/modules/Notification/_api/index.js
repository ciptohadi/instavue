import { HTTP } from '@/utils/http-common'

const getNotifications = (cursor = '', direction = '', limit = '', config = {}) => (
  HTTP.get(`notifications?cursor=${cursor}&direction=${direction}&limit=${limit}`, config)
    .then((response) => { return response })
    .catch((error) => { throw error })
)

const markNotificationAsRead = (id, cb, errCb, config = {}) => {
  HTTP.post(`notifications/${id}/mark-as-read`, [], config)
  .then(response => {
    cb(response)
  })
  .catch(error => {
    errCb(error)
  })
}

const markNotificationAsUnread = (id, cb, errCb, config = {}) => {
  HTTP.delete(`notifications/${id}/mark-as-unread`, config)
  .then(response => {
    cb(response)
  })
  .catch(error => {
    errCb(error)
  })
}

export default {
  getNotifications, markNotificationAsRead, markNotificationAsUnread
}
