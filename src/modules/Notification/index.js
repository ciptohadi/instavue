import NotificationList from './_components/NotificationList'
import NotificationListItem from './_components/NotificationListItem'
import CommentNotificationItem from './_components/CommentNotificationItem'
import LikeNotificationItem from './_components/LikeNotificationItem'
import FollowNotificationItem from './_components/FollowNotificationItem'
import store from '@/store'
import notificationStore from './_store'

if (!store.$_notification) store.registerModule('$_notification', notificationStore)

export { NotificationList, NotificationListItem, CommentNotificationItem, LikeNotificationItem, FollowNotificationItem }
