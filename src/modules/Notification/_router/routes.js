/* routes for photos feature */
import { loadComponent, checkAuth } from '@/utils/common'

const module = 'Notification'

export default [
  {
    path: '/notifications',
    name: 'notifications',
    component: loadComponent('Notification', module),
    beforeEnter: (to, from, next) => { checkAuth(to, from, next) }
  }
]
