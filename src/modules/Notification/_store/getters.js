const notifications = state => state.notifications
const completed = state => state.completed
const cursorDirection = state => state.cursor.direction

const getNotificationsStatus = state => state.getNotificationsStatus
const markNotificationAsReadStatus = state => state.markNotificationAsReadStatus
const markNotificationAsUnreadStatus = state => state.markNotificationAsUnreadStatus

export default {
  notifications,
  completed,
  cursorDirection,
  getNotificationsStatus,
  markNotificationAsReadStatus,
  markNotificationAsUnreadStatus
}
