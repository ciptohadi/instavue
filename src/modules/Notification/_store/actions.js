import * as types from './mutation-types'
import api from '../_api'

export default {
  getNotifications ({commit, state, rootState}) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }

    commit(types.GET_NOTIFICATIONS_REQUEST)

    setTimeout(() => {
      api.getNotifications(state.cursor.next, '', state.cursor.limit, { headers })
        .then((response) => {
          commit(types.GET_NOTIFICATIONS_SUCCESS, response)
        })
        .catch((error) => {
          commit(types.GET_NOTIFICATIONS_FAILURE, error)
        })
    }, 300)
  },

  getNotificationsRefresh ({commit, state, rootState}) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }

    commit(types.GET_NOTIFICATIONS_REFRESH_REQUEST)
    commit(types.GET_NOTIFICATIONS_REQUEST)

    setTimeout(() => {
      api.getNotifications('', '', state.cursor.limit, { headers })
        .then((response) => {
          commit(types.GET_NOTIFICATIONS_SUCCESS, response)
        })
        .catch((error) => {
          commit(types.GET_NOTIFICATIONS_FAILURE, error)
        })
    }, 300)
  },

  markNotificationAsRead ({ state, commit, rootState }, id) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }
    commit(types.MARK_NOTIFICATION_AS_READ_REQUEST)

    api.markNotificationAsRead(
      id,
      (response) => {
        commit(types.MARK_NOTIFICATION_AS_READ_SUCCESS, response)
        if (state.notifications) {
          commit(
            'UPDATE_STATE_OBJECT_IN_ARRAY',
            { module: '$_notification', stateObjectArrayKey: 'notifications', updatedObject: response.data.notification },
            { root: true }
          )
        }
      },
      (error) => {
        commit('ERROR_CAUGHT', null, { root: true })
        commit(types.MARK_NOTIFICATION_AS_READ_FAILURE, error)
      },
      { headers }
    )
  },

  markNotificationAsUnread ({ state, commit, rootState }, id) {
    let headers = {
      'Authorization': `Bearer ${rootState.authToken}`
    }
    commit(types.MARK_NOTIFICATION_AS_UNREAD_REQUEST)

    api.markNotificationAsUnread(
      id,
      (response) => {
        commit(types.MARK_NOTIFICATION_AS_UNREAD_SUCCESS, response)
        if (state.notifications) {
          commit(
            'UPDATE_STATE_OBJECT_IN_ARRAY',
            { module: '$_notification', stateObjectArrayKey: 'notifications', updatedObject: response.data.notification },
            { root: true }
          )
        }
      },
      (error) => {
        commit('ERROR_CAUGHT', null, { root: true })
        commit(types.MARK_NOTIFICATION_AS_UNREAD_FAILURE, error)
      },
      { headers }
    )
  }
}
