import * as types from './mutation-types'

export default {
  /* NOTIFICATION LIST */
  [types.GET_NOTIFICATIONS_REQUEST] (state) {
    state.getNotificationsStatus = null
  },

  [types.GET_NOTIFICATIONS_SUCCESS] (state, response) {
    state.getNotificationsStatus = 'Successfully get  all notifications!'

    let data = response.data

    if (state.completed) {
      return
    } else if (!data.notifications) {
      state.getNotificationsStatus = data.message.content
      state.completed = true
      if (data.meta) {
        state.cursor = Object.assign(state.cursor, {
          direction: data.meta.direction
        })
      }

      return
    }

    state.notifications = state.notifications.concat(data.notifications)
    state.cursor = Object.assign(state.cursor, {
      current: data.meta.cursor.current,
      next: data.meta.cursor.next,
      prev: data.meta.cursor.prev,
      count: data.meta.cursor.count
    })

    if (state.cursor.count < state.cursor.limit) {
      state.completed = true
    }
  },

  [types.GET_NOTIFICATIONS_FAILURE] (state, error) {
    let status = 'Something error. Try again or contact admin!'

    let data = error.response.data

    if (data.error) {
      status = data.error.message
    }

    state.getNotificationsStatus = status
  },

  [types.GET_NOTIFICATIONS_REFRESH_REQUEST] (state) {
    state.completed = false
    state.getNotificationsStatus = null
    state.notifications = []
  },

  /* MARK_NOTIFICATION_AS_READ_REQUEST */
  [types.MARK_NOTIFICATION_AS_READ_REQUEST] (state) {
    state.markNotificationAsReadStatus = null
  },

  [types.MARK_NOTIFICATION_AS_READ_SUCCESS] (state, response) {
    state.markNotificationAsReadStatus = response.data.message.content
  },

  [types.MARK_NOTIFICATION_AS_READ_FAILURE] (state, error) {
    let status = 'Something error. Try again or contact admin!'

    let data = error.response.data

    if (data.error) {
      status = data.error.message
    }

    state.markNotificationAsReadStatus = status
  },

  /* MARK_NOTIFICATION_AS_UNREAD_REQUEST */
  [types.MARK_NOTIFICATION_AS_UNREAD_REQUEST] (state) {
    state.markNotificationAsUnreadStatus = null
  },

  [types.MARK_NOTIFICATION_AS_UNREAD_SUCCESS] (state, response) {
    state.markNotificationAsUnreadStatus = response.data.message.content
  },

  [types.MARK_NOTIFICATION_AS_UNREAD_FAILURE] (state, error) {
    let status = 'Something error. Try again or contact admin!'

    let data = error.response.data

    if (data.error) {
      status = data.error.message
    }

    state.markNotificationAsUnreadStatus = status
  }
}
