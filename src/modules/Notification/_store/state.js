const stateNotificationList = {
  notifications: [],
  completed: false,
  cursor: {
    current: '',
    prev: '',
    next: '',
    limit: 7,
    count: 7,
    direction: ''
  },
  getNotificationsStatus: null
}

const stateMarkNotificationAsRead = {
  markNotificationAsReadStatus: null
}

const stateMarkNotificationAsUnread = {
  markNotificationAsUnreadStatus: null
}

export default Object.assign(
  stateNotificationList,
  stateMarkNotificationAsRead,
  stateMarkNotificationAsUnread
)
