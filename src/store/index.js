import Vue from 'vue'
import Vuex from 'vuex'
import * as accountMutationTypes from '@/modules/Account/_store/mutation-types'
import * as photoMutationTypes from '@/modules/Photo/_store/mutation-types'
import * as commentMutationTypes from '@/modules/Comment/_store/mutation-types'
import * as notificationMutationTypes from '@/modules/Notification/_store/mutation-types'
// import VuexPersistence from 'vuex-persist'

Vue.use(Vuex)

// root store
const state = {
  initialLoading: true,
  loading: false,
  dataChanged: {
    photos: false,
    photosSearch: false,
    photo: { id: 0 },
    user: { id: 0 },
    comments: false,
    childComments: false
  },
  authToken: null,
  error: false
}

// getters
const getters = {
  initialLoading: state => state.initialLoading,
  loading: state => state.loading,
  dataChanged: state => state.dataChanged,
  authToken: state => state.authToken,
  error: state => state.error
}

// mutations
const mutations = {
  'INITIAL_LOADING_START': (state) => {
    state.initialLoading = true
  },

  'INITIAL_LOADING_STOP' (state) {
    state.initialLoading = false
  },

  'LOADING_START' (state) {
    state.loading = true
  },

  'LOADING_STOP' (state) {
    state.loading = false
  },

  'DATA_CHANGED_SET' (state, newData) {
    state.dataChanged = Object.assign(state.dataChanged, newData)
  },

  'DATA_CHANGED_RESET' (state) {
    state.dataChanged = {
      photos: false,
      photosSearch: false,
      photo: { id: 0 },
      user: { id: 0 },
      comments: false,
      childComments: false
    }
  },

  'AUTH_TOKEN_SET' (state, response) {
    state.authToken = response.data.token
  },

  'AUTH_TOKEN_UNSET' (state) {
    state.authToken = null
  },

  'ERROR_CAUGHT' (state) {
    state.error = true
  },

  'ERROR_DISCARDED' (state) {
    state.error = false
  },

  'ADD_STATE_OBJECT_IN_ARRAY' (state, {module, stateObjectArrayKey, newObject}) {
    if (stateObjectArrayKey === 'user') {
      state[module][stateObjectArrayKey]['photos']['data'].unshift(newObject)
    } if (module === '$_photo') {
      state[module][stateObjectArrayKey].unshift(newObject)
    }
  },

  'REMOVE_STATE_OBJECT_IN_ARRAY' (state, {module, stateObjectArrayKey, deletedObject}) {
    if (stateObjectArrayKey === 'user') {
      let stateObjectArray = state[module][stateObjectArrayKey]['photos']['data']
      state[module][stateObjectArrayKey]['photos']['data'] = stateObjectArray.filter(obj => obj.id !== deletedObject.id)
    } if (module === '$_photo') {
      let stateObjectArray = state[module][stateObjectArrayKey]
      state[module][stateObjectArrayKey] = stateObjectArray.filter(obj => obj.id !== deletedObject.id)
    }
  },

  'UPDATE_STATE_OBJECT_IN_ARRAY' (state, {module, stateObjectArrayKey, updatedObject}) {
    let stateObjectArray = state[module][stateObjectArrayKey]
    let returnObject = null

    stateObjectArray.map((obj) => {
      if (obj.id === updatedObject.id) {
        returnObject = Object.assign(obj, updatedObject)
      } else {
        returnObject = obj
      }
      return returnObject
    })
  },

  'UPDATE_STATE_OBJECT' (state, {module, stateObjectKey, updatedObject}) {
    let stateObject = state[module][stateObjectKey]

    state[module][stateObjectKey] = Object.assign(stateObject, updatedObject)
  }
}

const checkLoadingState = store => {
  // called when the store is initialized
  store.subscribe((mutation, state) => {
    // called after every mutation.
    // The mutation comes in the format of `{ type, payload }`.

    if (state.initialLoading &&
      (mutation.type === `$_photo/${photoMutationTypes.GET_PHOTOS_REFRESH_PREQUEST}` ||
      mutation.type === `$_photo/${photoMutationTypes.GET_PHOTOS_BROWSE_REFRESH_PREQUEST}` ||
      mutation.type === `$_comment/${commentMutationTypes.RESET_COMMENTS}` ||
      mutation.type === `$_comment/${commentMutationTypes.RESET_CHILD_COMMENTS}` ||
      mutation.type === `$_notification/${notificationMutationTypes.GET_NOTIFICATIONS_REFRESH_REQUEST}`)
    ) {
      store.commit('INITIAL_LOADING_STOP')
    }

    let loadingStartAccountMutationTypes = getLoadingStartMutationTypes(accountMutationTypes)
    let loadingStartPhotoMutationTypes = getLoadingStartMutationTypes(photoMutationTypes)
    let loadingStartCommentMutationTypes = getLoadingStartMutationTypes(commentMutationTypes)
    let loadingStartNotificationMutationTypes = getLoadingStartMutationTypes(notificationMutationTypes)
    let loadingStartMutationTypes = [...loadingStartAccountMutationTypes, ...loadingStartPhotoMutationTypes, ...loadingStartCommentMutationTypes, ...loadingStartNotificationMutationTypes]

    let loadingStopAccountMutationTypes = getLoadingStopMutationTypes(accountMutationTypes)
    let loadingStopPhotoMutationTypes = getLoadingStopMutationTypes(photoMutationTypes)
    let loadingStoptCommentMutationTypes = getLoadingStopMutationTypes(commentMutationTypes)
    let loadingStopNotificationMutationTypes = getLoadingStopMutationTypes(notificationMutationTypes)
    let loadingStopMutationTypes = [...loadingStopAccountMutationTypes, ...loadingStopPhotoMutationTypes, ...loadingStoptCommentMutationTypes, ...loadingStopNotificationMutationTypes]

    if (loadingStartMutationTypes.includes(mutation.type)) {
      store.commit('LOADING_START')
    } else if (loadingStopMutationTypes.includes(mutation.type)) {
      store.commit('LOADING_STOP')
    } else if (mutation.type.includes('SET_PHOTO_TOBE')) {
      store.commit('ERROR_DISCARDED')
    }
  })
}

const getLoadingStartMutationTypes = (types) => {
  let loadingStartMutationTypes = Object.values(types).filter(type => type.includes('_REQUEST'))
  for (let i = 0; i < loadingStartMutationTypes.length; i++) {
    loadingStartMutationTypes[i] = `${types.MODULE_NAMESPACE}/${loadingStartMutationTypes[i]}`
  }
  return loadingStartMutationTypes
}

const getLoadingStopMutationTypes = (types) => {
  let loadingStopMutationTypes = Object.values(types).filter(type => ((!type.includes('LOGIN_ACCOUNT_SUCCESS') && type.includes('_SUCCESS')) || type.includes('_FAILURE')))
  for (let i = 0; i < loadingStopMutationTypes.length; i++) {
    loadingStopMutationTypes[i] = `${types.MODULE_NAMESPACE}/${loadingStopMutationTypes[i]}`
  }
  return loadingStopMutationTypes
}

// const vuexLocalStorage = new VuexPersistence({
//   key: 'vuexInstaclone',
//   storage: window.localStorage
// })

export default new Vuex.Store({
  state,
  getters,
  mutations,
  plugins: [checkLoadingState]
})
