/* root routes */

import { loadComponent, checkAuth } from '@/utils/common'
import userRoutes from '@/modules/Account/_router/routes'
import photoRoutes from '@/modules/Photo/_router/routes'
import commentRoutes from '@/modules/Comment/_router/routes'
import notificationRoutes from '@/modules/Notification/_router/routes'

export default [
  {
    path: '/',
    name: 'home',
    component: loadComponent('Home'),
    beforeEnter: (to, from, next) => { checkAuth(to, from, next) }
  },
  {
    path: '/browse',
    name: 'browse',
    component: loadComponent('Browse'),
    beforeEnter: (to, from, next) => { checkAuth(to, from, next) }
  },
  { path: '/welcome', name: 'welcome', component: loadComponent('Welcome') },
  { path: '/error', name: 'error', component: loadComponent('Error') },
  { path: '/404', name: 'notfound', component: loadComponent('NotFound') },
  { path: '*', redirect: { name: 'notfound' } },
  ...userRoutes,
  ...photoRoutes,
  ...commentRoutes,
  ...notificationRoutes
]
