import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'
import store from '@/store'

Vue.use(VueRouter)

/* utils for stringifyQuery */
const encodeReserveRE = /[!'()*]/g
const encodeReserveReplacer = function (c) { return '%' + c.charCodeAt(0).toString(16) }
const commaRE = /%2C/g

// fixed encodeURIComponent which is more conformant to RFC3986:
// - escapes [!'()*]
// - preserve commas
const encode = function (str) {
  return encodeURIComponent(str)
    .replace(encodeReserveRE, encodeReserveReplacer)
    .replace(commaRE, ',')
}

const router = new VueRouter({
  mode: 'history',
  scrollBehavior: (to, from, savedPosition) => {
    if (savedPosition) {
      return savedPosition
    }
    if (to.hash) {
      return { selector: to.hash }
    }
  },
  routes,

  stringifyQuery (obj) {
    var res = obj ? Object.keys(obj).map(function (key) {
      var val = obj[key]

      if (val === undefined) {
        return ''
      }

      if (val === null) {
        return encode(key)
      }

      if (Array.isArray(val)) {
        var result = []
        val.forEach(function (val2) {
          if (val2 === undefined) {
            return
          }
          if (val2 === null) {
            result.push(encode(key))
          } else {
            result.push(encode(key) + '=' + encode(val2))
          }
        })
        return result.join('&')
      }

      if (key !== 'cursor') {
        val = encode(val)
      }

      return encode(key) + '=' + val
    }).filter(function (x) { return x.length > 0 }).join('&') : null
    return res ? ('?' + res) : ''
  }
})

router.beforeEach((to, from, next) => {
  if (store.state.error) {
    store.commit('ERROR_DISCARDED')
  }
  next()
})

export default router
